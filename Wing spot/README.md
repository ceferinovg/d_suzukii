###############################

README

###############################

Here you can the scripts used in the article:

Drosophila suzukii wing spot size is robust to developmental temperature

Varón-González, Fraimout and Debat (2019). Ecology & Evolution

You can find the data in Dryad.

Before having a look at the files, please consider two things:

1) I've tried to upload the minimum amount of information neccesary to run all the analyses, in
order to make the replication as simple as possible and avoid possible sources of confusion.

However, during this effort I might have left out something important unintendedly. If that's the case,
or if you struggle to understand something, please don't hesitate to contact me.

2) In the plasticity analyses you might find a population with a different name: Sokol. This is the name
the people collecting the samples gave to the population obtained in Dayton (Oregon). Because I learnt this
some time after I started the project I left the former name in these files, although I used to correct
place name in the manuscript. Sorry about that.