###############################

README

###############################

Here you can find some scripts used in the article:

Limited thermal plasticity and geographic divergence in the ovipositor of Drosophila suzukii

Varón-González, Fraimout, Delapré, Debat & Cornëtte (2019) Royal Society Open Science

You can find the data in Dryad.

Then, a warning:

In the analyses you might find a population with a different name: Sokol. This is the name
the people collecting the samples gave to the population obtained in Dayton (Oregon). Because I learnt this
some time after I started the project I left the former name in these files, although we used to correct
place name in the manuscript. Sorry about that.